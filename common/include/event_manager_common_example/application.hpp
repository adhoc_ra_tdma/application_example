#ifndef __EVENT_MANAGER_EXAMPLE_APPLICATION_HPP__
#define __EVENT_MANAGER_EXAMPLE_APPLICATION_HPP__


#include <simulatable_sockets/simulatable_sockets.hpp>
#include <simulatable_timer/simulatable_timer.hpp>
#include <simulatable_clock/simulatable_clock.hpp>

class example_application
{
	public:


		example_application(uint16_t id, std::string ifname, uint16_t unicast_port, std::string multicast_address, uint16_t multicast_port);

		example_application(const example_application &other);
		example_application &operator=(const example_application &other);

		example_application(example_application &&other);
		example_application &operator=(example_application &&other);

		virtual ~example_application()=default;

		std::unique_ptr<common::simulation_interface::udp_sockets::unicast_socket_interface> sock;
		std::unique_ptr<common::simulation_interface::udp_sockets::multicast_socket_interface> msock;
		std::unique_ptr<common::simulation_interface::timer::timer_interface> timer;

		common::simulation_interface::simulation_clock::time_point initial_time=common::simulation_interface::simulation_clock::now();
};


#endif /* __MYUDP_APP_HPP__ */
