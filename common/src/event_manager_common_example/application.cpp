

#include <event_manager_common_example/application.hpp>

#include <iostream>
#include <chrono>


example_application::example_application(uint16_t id, std::string ifname, uint16_t unicast_port, std::string multicast_address, uint16_t multicast_port)
{

	// Create socket
	sock=common::simulation_interface::udp_sockets::new_udp_unicast_socket(unicast_port);


	common::network::generics::udp_endpoint ep(multicast_address, multicast_port);
	msock=common::simulation_interface::udp_sockets::new_udp_multicast_socket(ifname,ep);

	sock->start();
	msock->start();


	double idd=(double)id;

	timer=common::simulation_interface::timer::new_timer();
	std::chrono::milliseconds init((int)(1000+1000*(idd/6.0)));
	std::chrono::seconds period(1);
	std::cout<<"Starting at "<<init.count()<<std::endl;
	auto cb=[this, idd, unicast_port, multicast_address,multicast_port]()
	{
		std::cout<<"Firing timer on node "<<idd<<" at "<<
				   common::simulation_interface::simulation_clock::now().time_since_epoch().count()
				   <<std::endl;
		common::simulation_interface::simulation_clock::time_point d=common::simulation_interface::simulation_clock::now();

		if((d-initial_time)>=std::chrono::seconds(20))
		{
			this->timer->stop();
		}

		std::vector<uint8_t> data({1,2,3,4,5,6,7,8,9,10});

		if((d-initial_time)<std::chrono::seconds(10))
		{

			common::network::generics::MessageUDP m("192.168.1.2",unicast_port,data);
			sock->write(m);
		}
		else
		{
			common::network::generics::MessageUDP m(multicast_address,multicast_port,data);
			msock->write(m);

		}
	};

	timer->start(cb, init, period);

	sock->async_read([](const common::network::generics::MessageUDP &m)
	{
		std::cout<<"received message from"<<m.getUDPEndpoint().getAddr()<<":"<<m.getUDPEndpoint().getPort()<<"\n";
	});

	msock->async_read([](const common::network::generics::MessageUDP &m)
	{
		std::cout<<"received multicast message from"<<m.getUDPEndpoint().getAddr()<<":"<<m.getUDPEndpoint().getPort()<<"\n";
	});

}
