#ifndef __SERVERUDPAPP_HPP__
#define __SERVERUDPAPP_HPP__

#include <applications/common/ApplicationBase.h>

#include <event_manager_common_example/application.hpp>

class INET_API timer_and_socket_manager : public cSimpleModule
{
	protected:

	public:
		timer_and_socket_manager();
		~timer_and_socket_manager();

		virtual int numInitStages() const  {return 5;}

	protected:
		virtual void handleMessage(cMessage *msg);

		virtual void initialize(int stage);

		virtual void finish();

		example_application *app;

		cOutVector eventsFired;
};



#endif /* __MYUDP_APP_HPP__ */
