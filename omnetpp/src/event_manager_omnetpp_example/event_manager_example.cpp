#include <event_manager_omnetpp_example/event_manager_example.hpp>


Define_Module(timer_and_socket_manager);

timer_and_socket_manager::timer_and_socket_manager()
{

}

timer_and_socket_manager::~timer_and_socket_manager()
{
}

void timer_and_socket_manager::initialize(int stage)
{
	cSimpleModule::initialize(stage);

	if (stage == 4)
	{

		app=new example_application(getParentModule()->getParentModule()->getIndex(), "wlan0", par("uCastPort").longValue(), "224.16.32.39", 20000);

		eventsFired.setName("Manager events");
	}

}

void timer_and_socket_manager::finish()
{
	// Do something

	cSimpleModule::finish();
}

void timer_and_socket_manager::handleMessage(cMessage *msg)
{

}
