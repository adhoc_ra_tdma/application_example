

#include <event_manager_common_example/application.hpp>
#include <unistd.h>
#include <signal.h>

#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>
std::promise<void> end;

static void signal_catch(int sig)
{
	end.set_value();
}


int main(int argc, char **argv)
{


		if(signal(SIGINT, signal_catch) == SIG_ERR)
		{
			PERROR(std::cout,"signal");
			return -1;
		}


	//run with: ./exec id network_interface unicast_port multicast_address multicast_port
	example_application *app=new example_application(atoi(argv[1]), std::string(argv[2]), atoi(argv[3]), std::string(argv[4]), atoi(argv[5]));


	end.get_future().get();

	return 0;
}
